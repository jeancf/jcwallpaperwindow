#version 330
#pragma debug(on)
#pragma optimize(off)

smooth in vec4 interpolatedColor;
out vec4 finalColor;

void main(){
    finalColor = interpolatedColor;
}
