#version 330
#pragma debug(on)
#pragma optimize(off)

in vec4 vertices;
in vec4 vertexColor;
uniform mat4 transform;

smooth out vec4 interpolatedColor;

void main() {
    interpolatedColor = vertexColor;
    gl_Position = transform * vertices;
}
