/****************************************************************************
**
** Copyright (C) 2016 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** This file is part of the demonstration applications of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** BSD License Usage
** Alternatively, you may use this file under the terms of the BSD license
** as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of The Qt Company Ltd nor the names of its
**     contributors may be used to endorse or promote products derived
**     from this software without specific prior written permission.
**
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "cppscene.h"

#include <QtQuick/qquickwindow.h>
#include <QtGui/QOpenGLShaderProgram>
#include <QtGui/QOpenGLContext>
#include <QMatrix4x4>
#include <QtDebug>
#include <QTime>
#include <QString>
#include <qmath.h>

//! [7]
CppScene::CppScene()
    : m_t(0)
    , m_renderer(nullptr)
{
    connect(this, &QQuickItem::windowChanged, this, &CppScene::handleWindowChanged);
}

void CppScene::setT(qreal t)
{
    if (t == m_t)
        return;
    m_t = t;
    emit tChanged();
    if (window())
        window()->update();
}

// Slot associated to QQuickItem::windowChanged
// (emitted when QQuickItem is added to or removed from a window presumably -- no doc)
void CppScene::handleWindowChanged(QQuickWindow *win)
{
    if (win) {
        // Associate slots to signals from owning window
        connect(win, &QQuickWindow::beforeSynchronizing, this, &CppScene::sync, Qt::DirectConnection);
        connect(win, &QQuickWindow::sceneGraphInvalidated, this, &CppScene::cleanup, Qt::DirectConnection);

        // If we allow QML to clear the scene graph, they would clear what we paint
        // and nothing would show. This allows OpenGL to draw under the QML scene graph.
        win->setClearBeforeRendering(false);
    }
}

// Slot associated with QQuickWindow::sceneGraphInvalidated
// (emitted when Window is closed presumably --no documentation)
void CppScene::cleanup()
{
    if (m_renderer) {
        delete m_renderer;
        m_renderer = nullptr;
    }
}

// Slot associated to QQuickWindow::beforeSynchronizing
// (emitted when Window is created presumably --no documentation)
void CppScene::sync()
{
    if (!m_renderer) {
        // Initialize QOpenGLShaderProgram
        m_renderer = new CppSceneRenderer();
        // Run &CppSceneRenderer::paint every time QQuickWindow needs a refresh
        connect(window(), &QQuickWindow::beforeRendering, m_renderer, &CppSceneRenderer::paint, Qt::DirectConnection);
    }
    m_renderer->setViewportSize(window()->size() * window()->devicePixelRatio());
    m_renderer->setT(m_t);
    m_renderer->setWindow(window());
}

CppSceneRenderer::CppSceneRenderer() : m_t(0), m_timer(), m_program(nullptr)
{
}

CppSceneRenderer::~CppSceneRenderer()
{
    delete m_program;
}

void CppSceneRenderer::paint()
{
    if (!m_program) {
        initializeOpenGLFunctions();

        m_program = new QOpenGLShaderProgram();
        assert(m_program->addCacheableShaderFromSourceFile(QOpenGLShader::Vertex, ":/scene/shader.vert"));
        assert(m_program->addCacheableShaderFromSourceFile(QOpenGLShader::Fragment,":/scene/shader.frag"));

        // bind shader variable named "vertices" with location 0
        m_program->bindAttributeLocation("vertices", 0);
        m_program->bindAttributeLocation("vertexColor", 1);
        m_program->link();

        m_timer.start();
    }
    m_program->bind();

    // Load location 0 with content of 'values' array
    float values[] = {
        -0.5, -0.5,
        0, 0.5,
        0.5, -0.5,
    };
    m_program->setAttributeArray(0, GL_FLOAT, values, 2);
    m_program->enableAttributeArray(0);

    // Load location 1 with vertex colors
    float colors[] = {
        1.0, 0.0, 0.0, 1.0,
        0.0, 1.0, 0.0, 1.0,
        0.0, 0.0, 1.0, 1.0
    };
    m_program->setAttributeArray(1, GL_FLOAT, colors, 4);
    m_program->enableAttributeArray(1);

    // make 1 turn in 1000 milliseconds
    // angle is 2 * pi at every int multiple of 4000 milliseconds
    double a = 2.0 * M_PI * (m_timer.elapsed() / 4000.0 - qFloor(m_timer.elapsed() / 4000.0));

    //QString msg = QString::number(a, 'f', 4);
    //qDebug() << msg;

    // Bind transform matrix to be used in vertex shader
    QMatrix4x4 transformMatrix(
                qCos(a), -qSin(a), 0.0, 0.0,
                qSin(a),  qCos(a), 0.0, 0.0,
                0.0    ,      0.0, 1.0, 0.0,
                0.0    ,      0.0, 0.0, 1.0
                );
    m_program->setUniformValueArray("transform", &transformMatrix, 1);

    // bind shader uniform variable named "t" with C++ float variable m_t
    //m_program->setUniformValue("t", static_cast<float>(m_t));

    glViewport(0, 0, m_viewportSize.width(), m_viewportSize.height());

    glDisable(GL_DEPTH_TEST);

    glClearColor(0, 0, 0, 1);
    glClear(GL_COLOR_BUFFER_BIT);

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE);

    glDrawArrays(GL_TRIANGLE_STRIP, 0, 3);

    m_program->disableAttributeArray(0);
    m_program->disableAttributeArray(1);
    m_program->release();

    // Not strictly needed for this example, but generally useful for when
    // mixing with raw OpenGL.
    m_window->resetOpenGLState();
}
