#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include "./scene/cppscene.h"

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    qmlRegisterType<CppScene>("LocalTypes", 1, 0, "CppScene");

    QQmlApplicationEngine engine("qrc:///main.qml");

    return app.exec();
}
