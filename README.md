# jcWallpaperWindow #

Minimalistic QML application that provides the same context as the animated wallpaper
jcWallpaper but in a window instead of the Plasma desktop background.

The wallpaper can be built as a QML scene or a C++ scene.

Place code in `Scene.qml` or `cppscene.cpp` respectively.

When done, the file can be dropped in the jcWallpaper QML app for use as desktop background.

The master branch contains the template (drawing flat triangle in 3 colors). Other branches
contain different tests.
