import QtQuick 2.12
import QtQuick.Window 2.12
import LocalTypes 1.0

Window {
    visible: true
    width: 1024
    height: 768
    title: qsTr("jcWallpaperWindow")
    CppScene {
        SequentialAnimation on t {
            NumberAnimation { to: 1; duration: 2500; easing.type: Easing.InQuad }
            NumberAnimation { to: 0; duration: 2500; easing.type: Easing.OutQuad }
            loops: Animation.Infinite
            running: true
        }
    }
    //Scene {}
}
